package ua.danit.dao;


import javassist.NotFoundException;
import org.springframework.stereotype.Component;
import ua.danit.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class CommentDao {

    private final EntityManager manager;

    @Autowired
    public CommentDao(EntityManager manager) {
        this.manager = manager;
    }

    @Transactional
    public Optional<Comment> getById(long id) {
        Comment comment = manager.find(Comment.class, id);
        return Optional.ofNullable(comment);
    }

    public List getAll() {
        return manager.createQuery("from Comment").getResultList();
    }


    @Transactional
    public Comment add(Comment comment) {
        manager.persist(comment);
        return comment;
    }

    @Transactional
    public Comment update(Comment comment) {
        return manager.merge(comment);
    }

    @Transactional
    public Optional<Comment> remove(long id) throws IllegalArgumentException, NotFoundException {
        Comment found = manager.find(Comment.class, id);
            manager.remove(found);
            return Optional.ofNullable(found);
        }
    }


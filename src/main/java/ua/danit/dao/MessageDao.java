package ua.danit.dao;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.danit.model.Message;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@Component
public class MessageDao {

    private final EntityManager manager;

    @Autowired
    public MessageDao(EntityManager manager) {
        this.manager = manager;
    }

    @Transactional
    public Optional<Message> getById(long id) {
        Message message = manager.find(Message.class, id);
        return Optional.ofNullable(message);
    }

    public List<Message> getAll() {
        return manager.createQuery("from Message").getResultList();
    }


    @Transactional
    public Message add(Message message) {
        manager.persist(message);
        return message;
    }

    @Transactional
    public Message update(Message message) {
        return manager.merge(message);
    }

    @Transactional
    public Optional<Message> remove(long id) throws IllegalArgumentException, NotFoundException {
        Message found = manager.find(Message.class, id);
        manager.remove(found);
        return Optional.ofNullable(found);
    }
}



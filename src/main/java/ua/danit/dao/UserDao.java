package ua.danit.dao;

import javassist.NotFoundException;
import org.springframework.stereotype.Component;
import ua.danit.model.Comment;
import ua.danit.model.Follower;
import ua.danit.model.Post;
import ua.danit.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class UserDao {

  private final EntityManager manager;

  @Autowired
  public UserDao(EntityManager manager) {
    this.manager = manager;
  }

  @Transactional
  public Optional<User> getById(long id) {
    User user = manager.find(User.class, id);
    return Optional.ofNullable(user);
  }

  public List getAll() {
    return manager.createQuery("from User").getResultList();
  }


  @Transactional
  public User add(User user) {
    manager.persist(user);
    return user;
  }

  @Transactional
  public User update (User user) {
    return manager.merge(user);
  }

  @Transactional
  public Optional<User> remove(long id) {
      User found = manager.find(User.class, id);
        manager.remove(found);
        return Optional.ofNullable(found);
      }



  public List<Follower> getFollowersByUser(long id) {
    String hql = "from Follower where followedId = %d";
    return manager.createQuery(String.format(hql, id)).getResultList();
  }

  public Follower getFollowerByUserById(long id) {
    return manager.find(Follower.class, id);
  }

  public List<Post> getPostsByUser(long id) {
    String hql = "from Post where userId = %d";
    return manager.createQuery(String.format(hql,id)).getResultList();
  }

  public Post getPostByUserById(long id) {
    return manager.find(Post.class, id);
  }

  public List<Comment> getAllCommentsByPostsId(long id) {
    String hql = "from Comment where postId = %d";
    return manager.createQuery(String.format(hql, id)).getResultList();
  }
}

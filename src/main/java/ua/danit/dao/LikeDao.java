package ua.danit.dao;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.danit.model.Like;


import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class LikeDao {
    private final EntityManager manager;

    @Autowired
    public LikeDao(EntityManager manager) {
        this.manager = manager;
    }

    @Transactional
    public Optional<Like> getById(long idFirst, long idSecond) {
        Like like = manager.find(Like.class, new Like.LikePK(idFirst,idSecond));
        return Optional.ofNullable(like);
    }

    public List<Like> getAll() {
        return manager.createQuery("from Like").getResultList();
    }


    @Transactional
    public Like add(Like like) {
        manager.persist(like);
        return like;
    }

    @Transactional
    public Like update(Like like) {
        return manager.merge(like);
    }

    @Transactional
    public Optional<Like> remove(long idFirst, long idSecond) throws IllegalArgumentException, NotFoundException {
        Like found = manager.find(Like.class, new Like.LikePK(idFirst,idSecond));
            manager.remove(found);
            return Optional.ofNullable(found);
        }
    }


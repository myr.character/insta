package ua.danit.dao;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.danit.model.Follower;


import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class FollowerDao {

    private final EntityManager manager;

    @Autowired
    public FollowerDao(EntityManager manager) {
        this.manager = manager;
    }

    @Transactional
    public Optional<Follower> getById(long idFirst, long idSecond) {
        Follower follower = manager.find(Follower.class, new Follower.FollowerPK(idFirst,idSecond));
        return Optional.ofNullable(follower);
    }

    public List<Follower> getAll() {
        return manager.createQuery("from Follower").getResultList();
    }


    @Transactional
    public Follower add(Follower follower) {
        manager.persist(follower);
        return follower;
    }

    @Transactional
    public Follower update(Follower follower) {
       return manager.merge(follower);
    }

    @Transactional
    public Optional<Follower> remove(long idFirst, long idSecond)    {
        Follower found = manager.find(Follower.class, new Follower.FollowerPK(idFirst, idSecond));
            manager.remove(found);
            return Optional.ofNullable(found);
        }
    }


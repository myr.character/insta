package ua.danit.dao;

import javassist.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ua.danit.model.Post;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public class PostDao {

  private final EntityManager manager;

  @Autowired
  public PostDao(EntityManager manager) {
    this.manager = manager;
  }

  @Transactional
  public Optional<Post> getById(long id) {
    Post post = manager.find(Post.class, id);

    return Optional.ofNullable(post);
  }

  public List<Post> getAll() {
    return manager.createQuery("from Post").getResultList();
  }


  @Transactional
  public Post add(Post post) {
    manager.persist(post);
    return post;
  }

  @Transactional
  public Post update(Post post) {
    return manager.merge(post);
  }

  @Transactional
  public Optional<Post> remove(long id)  {
    Post found = manager.find(Post.class, id);
    if (found == null) {
      return null;
    } else {
      manager.remove(found);
      return Optional.ofNullable(found);
    }
    }
  }






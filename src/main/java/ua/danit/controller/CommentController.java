package ua.danit.controller;

import javassist.NotFoundException;
import org.springframework.web.bind.annotation.*;
import ua.danit.dao.CommentDao;
import ua.danit.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ua.danit.service.CommentService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

  private final CommentService commentService;

  @Autowired
  public CommentController(CommentService commentService) {
    this.commentService = commentService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<Comment> getById(@PathVariable("id") long id) {
    return commentService.getById(id)
        .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
        .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @GetMapping
  public ResponseEntity<List<Comment>> getAll() {
    return ResponseEntity.ok(commentService.getAll());
  }

  @PostMapping
  public ResponseEntity<Comment> create(Comment comment) {
    return ResponseEntity.ok(commentService.add(comment));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> remove(@PathVariable long id) throws NotFoundException {
    Optional<Comment> found = commentService.remove(id);
    return Optional.ofNullable(found)
            .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
            .orElseGet(() -> ResponseEntity.notFound().build());

  }

  @PutMapping("/{id}")
  public ResponseEntity<Comment> update( Comment comment, @PathVariable("id") long id) {
    comment.setId(id);
    comment.setTimestamp(System.currentTimeMillis());

    return ResponseEntity.ok(commentService.update(comment));
  }
}


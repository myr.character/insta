package ua.danit.controller;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.dao.FollowerDao;
import ua.danit.model.Follower;
import ua.danit.model.Like;
import ua.danit.service.FollowerService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/followers")
public class FollowerController {

    private final FollowerService followerService;

    @Autowired
    public FollowerController(FollowerService followerService) {
        this.followerService = followerService;
    }

    @GetMapping("/{idFirst}/{idSecond}")
    public ResponseEntity<Follower> getById(@PathVariable("idFirst") long idFirst, @PathVariable("idSecond") long idSecond) {
        return followerService.getById(idFirst,idSecond)
            .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
            .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<Follower>> getAll() {
        return ResponseEntity.ok(followerService.getAll());
    }

    @PostMapping
    public ResponseEntity<Follower> create(Follower follower) {
        return ResponseEntity.ok(followerService.add(follower));
    }

    @DeleteMapping("/{idFirst}/{idSecond}")
    public ResponseEntity<?> remove(@PathVariable("idFirst") long idFirst, @PathVariable("idSecond") long idSecond)  {
        Optional<Follower> found = followerService.remove(idFirst, idSecond);
        return Optional.ofNullable(found)
                .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{idFirst}/{idSecond}")
    public ResponseEntity<Follower> update( Follower follower, @PathVariable("idFirst") long idFirst, @PathVariable("idSecond") long idSecond) {

        follower.setFollowerPK(idFirst, idSecond);
        follower.setTimestamp(System.currentTimeMillis());

        return ResponseEntity.ok(followerService.update(follower));
    }
}

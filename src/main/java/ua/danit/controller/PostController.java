package ua.danit.controller;

import javassist.NotFoundException;
import org.springframework.web.bind.annotation.*;
import ua.danit.dao.PostDao;
import ua.danit.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ua.danit.service.PostService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/posts")
public class PostController {

  private final PostService postService;

  @Autowired
  public PostController(PostService postService) {
    this.postService = postService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<Post> getById(@PathVariable("id") long id) {
    return postService
            .getById(id)
            .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
            .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @GetMapping
  public ResponseEntity<List<Post>> getAll() {
    return ResponseEntity.ok(postService.getAll());
  }

  @PostMapping
  public ResponseEntity<Post> create(Post post) {
    return ResponseEntity.ok(postService.add(post));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> remove(@PathVariable long id)  {
    Optional<Post> found = postService.remove(id);
    return Optional.ofNullable(found)
        .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
        .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @PutMapping("/{id}")
  public ResponseEntity<Post> update(Post post, @PathVariable("id") long id) {

    post.setId(id);
    post.setTimestamp(System.currentTimeMillis());

    return ResponseEntity.ok(postService.update(post));
  }
}


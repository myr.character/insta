package ua.danit.controller;


import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.dao.MessageDao;
import ua.danit.model.Message;
import ua.danit.service.MessageService;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/messages")
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }


    @GetMapping("/{id}")
    public ResponseEntity<Message> getById(@PathVariable("id") long id) {
        return messageService
                .getById(id)
                .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<Message>> getAll() {
        return ResponseEntity.ok(messageService.getAll());
    }

    @PostMapping
    public ResponseEntity<Message> create( Message message) {
        return ResponseEntity.ok(messageService.add(message));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> remove(@PathVariable long id) throws NotFoundException {
        Optional<Message> found = messageService.remove(id);
        return Optional.ofNullable(found)
                .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Message> update( Message message, @PathVariable("id") long id) {

        message.setId(id);
        message.setTimestamp(System.currentTimeMillis());

        return ResponseEntity.ok(messageService.update(message));
    }
}

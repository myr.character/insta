package ua.danit.controller;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.dao.LikeDao;
import ua.danit.model.Like;
import ua.danit.service.LikeService;


import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/likes")
public class LikeController {

    private final LikeService likeService;

    @Autowired
    public LikeController(LikeService likeService) {
        this.likeService = likeService;
    }

    @GetMapping("/{idFirst}/{idSecond}")
    public ResponseEntity<Like> getById(@PathVariable("idFirst") long idFirst, @PathVariable("idSecond") long idSecond) {
        return likeService.getById(idFirst,idSecond)
            .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
            .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<Like>> getAll() {
        return ResponseEntity.ok(likeService.getAll());
    }

    @PostMapping
    public ResponseEntity<Like> create(Like like) {
        return ResponseEntity.ok(likeService.add(like));
    }

    @DeleteMapping("/{idFirst}/{idSecond}")
    public ResponseEntity<?> remove(@PathVariable("idFirst") long idFirst, @PathVariable("idSecond") long idSecond) throws NotFoundException {
        Optional<Like> found = likeService.remove(idFirst, idSecond);
        return Optional.ofNullable(found)
                .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{idFirst}/{idSecond}")
    public ResponseEntity<Like> update( Like like, @PathVariable("idFirst") long idFirst, @PathVariable("idSecond") long idSecond) {

        like.setLikePK(idFirst, idSecond);
        like.setTimestamp(System.currentTimeMillis());

        return ResponseEntity.ok(likeService.update(like));
    }
}

package ua.danit.controller;


import javassist.NotFoundException;
import org.springframework.web.bind.annotation.*;

import ua.danit.model.Comment;
import ua.danit.model.Follower;
import ua.danit.model.Post;
import ua.danit.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ua.danit.service.UserService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/users")
public class UserController {

  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<User> getById(@PathVariable("id") long id) {
    return userService.getById(id)
        .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
        .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @GetMapping
  public ResponseEntity<List<User>> getAll() {
    return ResponseEntity.ok(userService.getAll());
  }

  @PostMapping
  public ResponseEntity<User> create( User user) {
    return ResponseEntity.ok(userService.add(user));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> remove(@PathVariable long id)  {
    Optional<User> found = userService.remove(id);
    return Optional.ofNullable(found)
            .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
            .orElseGet(() -> ResponseEntity.notFound().build());

  }

  @PutMapping
  public ResponseEntity<User> update(User user) {
    return ResponseEntity.ok(userService.update(user));
  }

  @GetMapping("/{id}/followers")
  public ResponseEntity<List<Follower>> getFollowersByUser(@PathVariable("id") long id) {
    return ResponseEntity.ok(userService.getFollowersByUser(id));
  }

  @GetMapping("/{userId}/followers/{followerId}")
  public ResponseEntity<Follower> getFollowerByUserById (@PathVariable("followerId") long followerId) {
    return ResponseEntity.ok(userService.getFollowerByUserById(followerId));
  }

  @GetMapping("/{id}/posts")
  public ResponseEntity<List<Post>> getPostsByUser(@PathVariable("id") long id) {
    return ResponseEntity.ok(userService.getPostsByUser(id));
  }

  @GetMapping("/{user}/posts/{id}")
  public ResponseEntity<Post> getPostByUserById(@PathVariable("id") long id) {
    return ResponseEntity.ok(userService.getPostByUserById(id));
  }

  @GetMapping("/{userId}/posts/{id}/comments")
  ResponseEntity<List<Comment>> getAllCommentsByPostsId(@PathVariable("id") long id) {
    return ResponseEntity.ok(userService.getAllCommentsByPostsId(id));
  }







}




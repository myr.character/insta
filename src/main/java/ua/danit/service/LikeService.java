package ua.danit.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.LikeDao;
import ua.danit.model.Like;

import java.util.List;
import java.util.Optional;

@Service
public class LikeService {

    @Autowired
    private LikeDao likeDao;

    public Optional<Like> getById(long idFirst, long idSecond) {
        return likeDao.getById(idFirst, idSecond);
    }

    public List<Like> getAll() {
        return likeDao.getAll();
    }

    public Like add(Like like) {
        return likeDao.add(like);
    }

    public Like update(Like like) {
        return likeDao.update(like);
    }

    public Optional<Like> remove(long idFirst, long idSecond) throws NotFoundException {
        return likeDao.remove(idFirst,idSecond);
    }
}

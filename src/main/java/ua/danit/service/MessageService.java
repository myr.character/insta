package ua.danit.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.MessageDao;
import ua.danit.model.Message;

import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    @Autowired
    private MessageDao messageDao;

    public Optional<Message> getById(long id) {
        return messageDao.getById(id);
    }

    public List<Message> getAll() {
        return messageDao.getAll();
    }

    public Message add(Message message) {
        return messageDao.add(message);
    }

    public Message update(Message message) {
        return messageDao.update(message);
    }

    public Optional<Message> remove(long id) throws NotFoundException {
        return messageDao.remove(id);
    }
}

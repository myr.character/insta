package ua.danit.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.FollowerDao;
import ua.danit.model.Follower;

import java.util.List;
import java.util.Optional;

@Service
public class FollowerService {

    @Autowired
    private FollowerDao followerDao;

    public Optional<Follower> getById(long idFirst, long idSecond) {
        return followerDao.getById(idFirst, idSecond);
    }

    public List<Follower> getAll() {
        return followerDao.getAll();
    }

    public Follower add(Follower follower) {
        return followerDao.add(follower);
    }

    public Follower update(Follower follower) {
        return followerDao.update(follower);
    }

    public Optional<Follower> remove(long idFirst, long idSecond)  {
        return followerDao.remove(idFirst, idSecond);
    }

}

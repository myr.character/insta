package ua.danit.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.PostDao;
import ua.danit.model.Post;

import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    @Autowired
    private PostDao postDao;


    public Optional<Post> getById(long id) {
        return postDao.getById(id);
    }

    public List<Post> getAll() {
        return postDao.getAll();
    }

    public Post add(Post post) {
        return postDao.add(post);
    }

    public Post update(Post post) {
        return postDao.update(post);
    }

    public Optional<Post> remove(long id) {
        return postDao.remove(id);
    }
}

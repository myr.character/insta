package ua.danit.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.CommentDao;
import ua.danit.model.Comment;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    @Autowired
    private CommentDao commentDao;

    public Optional<Comment> getById(long id) {
        return commentDao.getById(id);
    }

    public List getAll() {
        return commentDao.getAll();
    }

    public Comment add(Comment comment) {
        return commentDao.add(comment);
    }

    public Comment update (Comment comment) {
        return commentDao.update(comment);
    }

    public Optional<Comment> remove(long id) throws NotFoundException {
        return commentDao.remove(id);
    }

}

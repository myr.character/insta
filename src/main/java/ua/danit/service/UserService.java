package ua.danit.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.UserDao;
import ua.danit.model.Comment;
import ua.danit.model.Follower;
import ua.danit.model.Post;
import ua.danit.model.User;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public Optional<User> getById(long id) {
        return userDao.getById(id);
    }
    public List getAll() {
        return userDao.getAll();
    }

    public User add(User user) {
        return userDao.add(user);
    }

    public User update (User user) {
        return userDao.update(user);
    }

    public Optional<User> remove(long id)  {
        return userDao.remove(id);
    }
    public List<Follower> getFollowersByUser(long id) {
        return userDao.getFollowersByUser(id);
    }

    public Follower getFollowerByUserById(long id) {
        return userDao.getFollowerByUserById(id);
    }

    public List<Post> getPostsByUser(long id) {
        return userDao.getPostsByUser(id);
    }

    public Post getPostByUserById(long id) {
        return userDao.getPostByUserById(id);
    }

    public List<Comment> getAllCommentsByPostsId(long id) { return  userDao.getAllCommentsByPostsId(id); }


}


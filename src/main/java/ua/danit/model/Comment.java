package ua.danit.model;

import javax.persistence.*;

@Entity
@Table(name = "INSTAGRAM_COMMENTS")
public class Comment {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "POST_ID")
  private long postId;

  @Column(name = "USER_ID")
  private long userId;

  @Column(name = "TEXT")
  private String text;

  @Column(name = "TIMESTAMP")
  private long timestamp;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getPostId() {
    return postId;
  }

  public long getUserId() {
    return userId;
  }

  public String getText() {
    return text;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }
}

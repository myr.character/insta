package ua.danit.model;


import javax.persistence.*;

@Entity
@Table(name = "INSTAGRAM_MESSAGES")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "SENDER_ID")
    private long senderId;

    @Column(name = "RECEIVER_ID")
    private long receiverId;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "TIMESTAMP")
    private long timestamp;


    public Message(long senderId, long receiverId, String text, long timestamp) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.text = text;
        this.timestamp = timestamp;
    }

    public Message() {
    }

    public long getId() {
        return id;
    }

    public long getSenderId() {
        return senderId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public String getText() {
        return text;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", senderId=" + senderId +
                ", receiverId=" + receiverId +
                ", text='" + text + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}

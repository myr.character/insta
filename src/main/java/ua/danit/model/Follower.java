package ua.danit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "INSTAGRAM_FOLLOWERS")
public class Follower implements Serializable   {

    @EmbeddedId
    private FollowerPK followerPK;

    @Column(name = "TIMESTAMP")
    private long timestamp;


    public Follower(FollowerPK followerPK, long timestamp) {
        this.followerPK = followerPK;
        this.timestamp = timestamp;
    }

    public Follower() {
    }

    public FollowerPK getFollowerPK() {
        return followerPK;
    }

    public void setFollowerPK(long idFirst, long idSecond ) {
        this.followerPK = new FollowerPK(idFirst, idSecond);
    }



    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Follower{" +
            "followerPK=" + followerPK +
            ", timestamp=" + timestamp +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Follower follower = (Follower) o;

        if (timestamp != follower.timestamp) return false;
        return followerPK != null ? followerPK.equals(follower.followerPK) : follower.followerPK == null;
    }

    @Override
    public int hashCode() {
        int result = followerPK != null ? followerPK.hashCode() : 0;
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }

    @Embeddable
    public static class  FollowerPK implements Serializable    {

        private long followerId;
        private long followedId;

        public FollowerPK() {
        }

        public FollowerPK(long followerId, long followedId) {
            this.followerId = followerId;
            this.followedId = followedId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FollowerPK that = (FollowerPK) o;
            return followerId == that.followerId &&
                followedId == that.followedId;
        }

        @Override
        public int hashCode() {

            return Objects.hash(followerId, followedId);
        }
    }

}


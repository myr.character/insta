package ua.danit.model;


import javax.persistence.*;

@Entity
@Table(name = "INSTAGRAM_POSTS")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "USER_ID")
    private long userId;

    @Column(name = "DESCRIPTION")
    private String desciption;

    @Column(name = "TIMESTAMP")
    private long timestamp;

    public Post(String desciption, long timestamp) {
        this.desciption = desciption;
        this.timestamp = timestamp;
    }

    public Post() {
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public String getDesciption() {
        return desciption;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", userId=" + userId +
                ", desciption='" + desciption + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}

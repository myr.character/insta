package ua.danit.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "INSTAGRAM_LIKES")
public class Like implements Serializable  {

    @EmbeddedId
    private LikePK likePK;

    @Column(name = "TIMESTAMP")
    private long timestamp;

  public Like(LikePK likePK, long timestamp) {
    this.likePK = likePK;
    this.timestamp = timestamp;
  }

  public Like(long idFirst, long idSecond, long timestamp) {
    this.likePK = new LikePK(idFirst, idSecond);
  }

  public Like() {
    }

  public LikePK getLikePK() {
    return likePK;
  }



  public void setLikePK(long idFirst, long idSecond) {
    this.likePK = new LikePK(idFirst, idSecond);
  }



  public long getTimestamp() {
    return timestamp;
  }

  public long getLikedId(){
    return this.likePK.likerId;
  }

  public long getLikedPostId(){
    return this.likePK.likedPostId;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  @Embeddable
  public static class LikePK implements Serializable {

    @Column(name = "LIKER_ID")
    private long likerId;

    @Column(name = "LIKED_POST_ID")
    private long likedPostId;

    public LikePK(long likerId, long likedPostId) {
      this.likerId = likerId;
      this.likedPostId = likedPostId;
    }

    public LikePK() {
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      LikePK likePK = (LikePK) o;

      if (likerId != likePK.likerId) return false;
      return likedPostId == likePK.likedPostId;
    }

    @Override
    public int hashCode() {
      int result = (int) (likerId ^ (likerId >>> 32));
      result = 31 * result + (int) (likedPostId ^ (likedPostId >>> 32));
      return result;
    }
  }
}


